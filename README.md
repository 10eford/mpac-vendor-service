
## MPAC Vendor Service

### Requirements

* JDK 8
* Maven: `brew install maven`

### Build & run unit tests

* `mvn package`

### Build a Docker image

TODO

### Run with in-memory database

* `java -jar target/mpac-vendorservice-1.0.jar profiles=default,dev`

### Run with MySQL database

Install and launch MySQL:

* `brew install mysql`
* `mysql.server start`

Create a database schema:

* `mysql -uroot -e "create schema vendorservice"`

Run the service using the `mysql` profile:

* `java -jar target/mpac-vendorservice-1.0.jar profiles=mysql`

### Endpoints

The service runs on port 8080.

* `GET /accounts` - Fetch all accounts.
* `GET /accounts/{id}` - Fetch single account.
* `POST /accounts` - Create account.
* `PUT /accounts/{id}` - Update account.
* `DELETE /accounts/{id}` - Delete account.
* `GET /accounts/{id}/contacts` - Fetch contacts by account ID.
* `GET /contacts` - Fetch all contacts.
* `GET /contacts/{id}` - Fetch single contact.
* `POST /contacts` - Create contact.
* `PUT /contacts/{id}` - Update contact.
* `DELETE /contacts/{id}` - Delete contact.

### Examples

##### Create account:
```
curl --request POST \
  --url http://localhost:8080/accounts \
  --header 'content-type: application/json' \
  --data '{
	"companyName": "Company Name"
}'
```

##### Get all accounts:
```
curl http://localhost:8080/accounts
```

##### Get single account:
```
curl http://localhost:8080/accounts/1
```

##### Update account:
```
curl --request PUT \
  --url http://localhost:8080/accounts/1 \
  --header 'content-type: application/json' \
  --data '{
	"companyName": "Company Name",
	"address1": "1 Main St"
}'
```

##### Create contact:
```
curl --request POST \
  --url http://localhost:8080/contacts \
  --header 'content-type: application/json' \
  --data '{
	"name": "Contact Name",
	"email": "name@domain.com",
	"accountId": 1
}'
```

##### Get all contacts:
```
curl http://localhost:8080/contacts
```

##### Get single contact:
```
curl http://localhost:8080/contacts/1
```

##### Get contacts by account ID:
```
curl http://localhost:8080/accounts/1/contacts
```

##### Update contact:
```
curl --request PUT \
  --url http://localhost:8080/contacts/1 \
  --header 'content-type: application/json' \
  --data '{
	"name": "Removed Account ID",
	"email": "name@domain.com"
}'
```

##### Delete account:
```
curl --request DELETE \
  --url http://localhost:8080/accounts/1 \
  --header 'content-type: application/json'
```

##### Delete contact:
```
curl --request DELETE \
  --url http://localhost:8080/contacts/1 \
  --header 'content-type: application/json'
```
