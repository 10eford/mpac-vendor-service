package com.atlassian.mpac.vendorservice;

import org.junit.Test;
import org.rapidoid.annotation.IntegrationTest;
import org.rapidoid.data.JSON;
import org.rapidoid.http.Self;
import org.rapidoid.test.RapidoidIntegrationTest;

import java.util.Map;

@IntegrationTest(main = ServiceMain.class)
public class ServiceTest extends RapidoidIntegrationTest {

    private static final Account[] testAccounts = {
            new Account(
                    1L,
                    "Company1",
                    "Address1",
                    "Address2",
                    "City",
                    "State",
                    "PostalCode",
                    "Country"
            ),
            new Account(
                    2L,
                    "Company2",
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
            )
    };

    private static final Contact[] testContacts = {
            new Contact(
                    1L,
                    "Name1",
                    "Email1",
                    1L,
                    "Address1",
                    "Address2",
                    "City",
                    "State",
                    "PostalCode",
                    "Country"
            ),
            new Contact(
                    2L,
                    "Name2",
                    "Email2",
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
            ),
            new Contact(
                    3L,
                    "Name3",
                    "Email3",
                    2L,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
            ),
            new Contact(
                    4L,
                    "Name4",
                    "Email4",
                    1L,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
            ),
    };

    @SuppressWarnings("unchecked")
    private Map<String, Object> toMap(Object object) {
        return JSON.MAPPER.convertValue(object, Map.class);
    }

    private <T> T get(String url, Class<T> valueType) {
        return JSON.parse(Self.get(url).fetch(), valueType);
    }

    private void put(String url, Map<String, Object> data) {
        Self.put(url).data(data).execute();
    }

    private void post(String url, Map<String, Object> data) {
        Self.post(url).data(data).execute();
    }

    private void delete(String url) {
        Self.delete(url).execute();
    }

    @Test
    public void test() {

        // Verify initial (empty) state.

        isTrue(get("/accounts", Account[].class).length == 0);
        isTrue(get("/contacts", Contact[].class).length == 0);

        // Create accounts and contacts

        for (Account a : testAccounts) {
            final Map<String, Object> data = toMap(a);
            data.remove("id");
            post("/accounts", data);
        }

        for (Contact c : testContacts) {
            final Map<String, Object> data = toMap(c);
            data.remove("id");
            post("/contacts", data);
        }

        // Get individual accounts and contacts

        for (int i = 0; i < testAccounts.length; i++) {
            final Account a = get("/accounts/" + (i + 1), Account.class);
            eq(testAccounts[i], a);
        }

        for (int i = 0; i < testContacts.length; i++) {
            final Contact c = Self.get("/contacts/" + (i + 1)).toBean(Contact.class);
            eq(testContacts[i], c);
        }

        // Get all accounts and contacts

        eq(testAccounts, get("/accounts", Account[].class));
        eq(testContacts, get("/contacts", Contact[].class));

        // Get contacts by account ID

        eq(new Contact[] { testContacts[0], testContacts[3] },
                get("/accounts/1/contacts", Contact[].class));

        eq(new Contact[] { testContacts[2] },
                get("/accounts/2/contacts", Contact[].class));

        // Update account

        {
            final Account account = testAccounts[1];
            account.city = "Austin";
            put("/accounts/2", toMap(account));
            eq(account, get("/accounts/2", Account.class));
        }

        // Update contact

        {
            final Contact contact = testContacts[2];
            contact.city = "Austin";
            put("/contacts/3", toMap(contact));
            eq(contact, get("/contacts/3", Contact.class));
        }

        // Delete accounts and contacts

        for (Account a : testAccounts) {
            delete("/accounts/" + a.id);
        }

        for (Contact c : testContacts) {
            delete("/contacts/" + c.id);
        }

        isTrue(get("/accounts", Account[].class).length == 0);
        isTrue(get("/contacts", Contact[].class).length == 0);
    }
}
