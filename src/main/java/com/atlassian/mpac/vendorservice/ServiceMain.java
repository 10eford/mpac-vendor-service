package com.atlassian.mpac.vendorservice;

import org.rapidoid.annotation.Valid;
import org.rapidoid.jpa.JPA;
import org.rapidoid.jpa.JPQL;
import org.rapidoid.setup.App;
import org.rapidoid.setup.On;

public class ServiceMain {

    private static final JPQL getContactsByAccountId = JPA.jpql(
            "select c from Contact c where accountId = ?1");

    public static void main(String[] args) {
        App.init(args);
        App.boot().beans().jpa().auth();

        // Get all accounts
        On.get("/accounts").json(() -> JPA.of(Account.class).all());

        // Get single account
        On.get("/accounts/{id}").json((Long id) -> JPA.get(Account.class, id));

        // Get contacts by account ID
        On.get("/accounts/{id}/contacts").json((Long id) -> getContactsByAccountId.bind(id));

        // Create account
        On.post("/accounts").json((@Valid Account a) -> JPA.save(a));

        // Update account
        On.put("/accounts/{id}").json((Long id, @Valid Account a) -> JPA.update(a));

        // Delete account
        On.delete("/accounts/{id}").json((Long id) -> {
            JPA.delete(Account.class, id);
            return true;
        });

        // Get all contacts
        On.get("/contacts").json(() -> JPA.of(Contact.class).all());

        // Get single contact
        On.get("/contacts/{id}").json((Long id) -> JPA.get(Contact.class, id));

        // Create contact
        On.post("/contacts").json((@Valid Contact c) -> JPA.save(c));

        // Update contact
        On.put("/contacts/{id}").json((Long id, @Valid Contact c) -> JPA.update(c));

        // Delete contact
        On.delete("/contacts/{id}").json((Long id) -> {
            JPA.delete(Contact.class, id);
            return true;
        });

        App.ready();
    }
}
