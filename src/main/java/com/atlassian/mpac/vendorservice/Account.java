package com.atlassian.mpac.vendorservice;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
public class Account {

    @Id
    @GeneratedValue
    public Long id;

    @NotNull
    public String companyName;

    public String address1;
    public String address2;
    public String city;
    public String state;
    public String postalCode;
    public String country;

    public Account() {}

    public Account(Long id, String companyName, String address1, String address2, String city, String state,
                   String postalCode, String country) {
        this.id = id;
        this.companyName = companyName;
        this.address1 = address1;
        this.address2 = address2;
        this.city = city;
        this.state = state;
        this.postalCode = postalCode;
        this.country = country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(id, account.id) &&
                Objects.equals(companyName, account.companyName) &&
                Objects.equals(address1, account.address1) &&
                Objects.equals(address2, account.address2) &&
                Objects.equals(city, account.city) &&
                Objects.equals(state, account.state) &&
                Objects.equals(postalCode, account.postalCode) &&
                Objects.equals(country, account.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, companyName, address1, address2, city, state, postalCode, country);
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", companyName='" + companyName + '\'' +
                ", address1='" + address1 + '\'' +
                ", address2='" + address2 + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", postalCode='" + postalCode + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
