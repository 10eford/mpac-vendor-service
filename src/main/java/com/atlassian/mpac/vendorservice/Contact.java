package com.atlassian.mpac.vendorservice;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
public class Contact {

    @Id
    @GeneratedValue
    public Long id;

    @NotNull
    public String name;

    @NotNull
    public String email;

    public Long accountId;
    public String address1;
    public String address2;
    public String city;
    public String state;
    public String postalCode;
    public String country;

    public Contact() {}

    public Contact(Long id, String name, String email, Long accountId, String address1, String address2,
                   String city, String state, String postalCode, String country) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.accountId = accountId;
        this.address1 = address1;
        this.address2 = address2;
        this.city = city;
        this.state = state;
        this.postalCode = postalCode;
        this.country = country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contact contact = (Contact) o;
        return Objects.equals(id, contact.id) &&
                Objects.equals(name, contact.name) &&
                Objects.equals(email, contact.email) &&
                Objects.equals(accountId, contact.accountId) &&
                Objects.equals(address1, contact.address1) &&
                Objects.equals(address2, contact.address2) &&
                Objects.equals(city, contact.city) &&
                Objects.equals(state, contact.state) &&
                Objects.equals(postalCode, contact.postalCode) &&
                Objects.equals(country, contact.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, email, accountId, address1, address2, city, state, postalCode, country);
    }

    @Override
    public String toString() {
        return "Contact{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", accountId=" + accountId +
                ", address1='" + address1 + '\'' +
                ", address2='" + address2 + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", postalCode='" + postalCode + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
